﻿using System;

namespace Lesson_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            #region 定义变量
            // 1. 有符号的整型变量，能存储一定范围内正负数包括0的变量类型
            // sbyte : -127~128
            sbyte sb = 1;
            Console.WriteLine($"sbyte变量中存储的值是：{sb}");
            // int : -21多~21亿多
            int i = 2;
            // short -32768~32767
            short s = 3;
            // long: -9九百万兆~9百万兆之间的数
            long l = 4;

            // 2. 无符号的整型变量存储一定范围0和正数的变量类型
            // byte: 0~255
            byte b = 1;
            // uint 0~42亿多的范围
            uint ui = 2;
            // ushort: 0~65535之间的一个数
            ushort us = 3;
            // ulong: 0~18百万兆之间的数
            ulong ul = 4;

            // 3.浮点数(小数)
            // float 存储7/8位有效数字，根据编译器不同，有效数字也可能不同，四舍五入
            // 有效数字是从左到右非0数字开始计算有效数字的位数
            float f = 0.1234567890f;
            Console.WriteLine($"结果有效数字：{f}");
            // double 存储15-17位有效数字，如果不加f，默认是double类型，废弃的数字也会四舍五入
            double d = 0.123456789123455;
            // decimal 存储27~28位的有效数字，不建议使用
            decimal de = 0.12464727832778727378283m;

            // 4.特殊类型
            // 布尔类型，表真假
            bool b1 = true;
            bool b2 = false;
            // 字符类型
            char c = '陈';
            // 字符串类型，用来存储多个字符，没有上限
            string name = "faksjdfkalfjsaklfjasklfj";

            #endregion

            #region 为什么有那么多变量类型
            // 不同的变量，存储的范围和类型不一样，本质是占用的内存空间不同
            // 选择不同的数据(变量)类型装载不同的数据
            // 就比如说年龄基本都是100以下
            byte age = 20;
            // 体重
            float weight = 68.5f;
            // 身高
            float height = 177.8f;
            // 性别
            char sex = '男';
            // 基本来说，数字用int，小数用float，字符串用string，真假用bool
            #endregion

            #region 多个相同类型变量，同时申明
            int a1 = 1, a2 = 2;
            float f1 = 0.12f, f2 = 0.23f;
            string s1 = "fsafdsa", s2 = "fasdfafa";
            #endregion

            #region 变量初始化相关
            // 变量申明时可以不赋初值，但在使用前必须得赋值
            int a;
            a = 1;
            Console.WriteLine(a);
            #endregion
        }

        static void CodeFold()
        {
            // 用于折叠功能代码块，更易理解程序整体流程
            // #region 和 #endregion 配套使用，可以给当前折叠的功能代码块命名
            // 本质是 编辑器提供给我们的 预处理指令
            // 只会在编辑时有用，发布代码后或执行代码时，会被自动删除，所以只是用来预览
            #region 折叠代码测试
            Console.WriteLine("代码折叠测试");
            #endregion
        }
    }
}
