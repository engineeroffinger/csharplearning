﻿using System;

namespace Lesson_5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            #region 常量的声明
            // const 变量类型 变量名 = 初始值;
            const int age = 10;
            #endregion

            #region 常量的特点
            // 1.必须初始化
            // 2.不能被修改
            // 作用就是申明一些常用不变的变量
            const float PI = 3.1415926f;
            #endregion
        }
    }
}
