﻿using System;

namespace First_APPlication
{
    abstract class Father
    {
        abstract void Speak();

        public virtual void Walk()
        {
            Console.WriteLine("父亲走路");
        }

        public void Eat()
        {
            Console.WriteLine("父亲吃饭");
        }
    }

    class Child : Father
    {
        public override void Speak()
        {
            Console.WriteLine("孩子说话");
        }

        public override void Walk()
        {
            base.Walk();
            Console.WriteLine("孩子走路");
        }

        public override void Eat()
        {
            Console.WriteLine("孩子吃饭");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Father father = new Child();
            father.Speak();
            father.Eat();
            father.Walk();

            Console.WriteLine("------------------------------");

            Child child = new Child();
            child.Speak();
            child.Eat();
            child.Walk();

        }

        static void TestWork()
        {
            Console.WriteLine("请输入姓名：");
            string name = Console.ReadLine();
            Console.WriteLine("请输入年龄：");
            string age = Console.ReadLine();
            Console.WriteLine("请输入性别：");
            string sex = Console.ReadLine();
            Console.WriteLine($"玩家的名字：{name}--年龄：{age}--性别：{sex}");
        }
        static void TestLearning()
        {
            Console.WriteLine("Hello World!");
            Console.WriteLine("请输入信息：");
            Console.ReadLine();
            Console.WriteLine("玩家输入完毕！");
            if (Console.ReadKey() != null)
            {
                Console.WriteLine("\n玩家按下了按键");
            }
        }
    }
}
