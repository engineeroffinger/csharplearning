﻿using System;

namespace Lesson_6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("转义字符");
            #region 转义字符的使用
            // 什么是转义字符
            // 它是字符串的一部分，用来表示一些特殊含义的字符
            // 比如：在字符串中表示 单引号 引号 空格等
            string test = "fasdfsa fasdfaf";
            #endregion

            #region 固定写法
            // 固定写法 \字符
            // \和不同的字符组合表示不同的含义
            // 常用转义字符
            //单引号 \`
            string str1 = "fafdsa\'";
            Console.WriteLine(str1);
            // 双引号 \"
            string str2 = "fasdfaf\"";
            Console.WriteLine(str2);
            // 换行符 \n
            string str3 = "Lfdsafaf\nfdsafa";
            Console.WriteLine(str3);
            // 斜杠 \\
            string str4 = "fafkadsf\\";
            Console.WriteLine(str4);

            // 不常用的转义字符
            // 制表符 \t 等价于一个tab键
            string str5 = "Lfdasfa\tfasfa";
            Console.WriteLine(str5);
            // 光标退格
            string str6 = "123\b123"; // 输出 12123
            Console.WriteLine(str6);
            // 空字符
            string str7 = "\0";  // 什么都没有
            Console.WriteLine(str7);
            // 警报音
            string str8 = "\a"; // 电脑会响一声
            Console.WriteLine(str8);
            #endregion

            #region 取消转义字符
            // 字符串前面加上@符
            string str9 = @"fasfda\fasdfa"; 
            #endregion
        }
    }
}
