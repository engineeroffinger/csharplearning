﻿using System;

namespace Lesson_3
{
    class Program
    {
        static void Main(string[] args)
        {
            #region 变量的存储空间(内存中)
            // 1byte = 8bit
            // 1KB = 1024byte
            // 1MB = 1024KB
            // 1GB = 1024MB
            // 1TB = 1024GB
            // 可以通过sizeof这个方法获取到该变量类型所占用的内存空间(单位：字节)
            int sbyteSize = sizeof(sbyte);
            Console.WriteLine($"sbyte 所占的字节数：" + sbyteSize);
            int intSize = sizeof(int);
            Console.WriteLine($"int 所占的字节数：" + intSize);
            int shortSize = sizeof(short);
            Console.WriteLine($"short 所占的字节数：" + shortSize);
            int longSize = sizeof(long);
            Console.WriteLine($"long 所占的字节数：" + longSize);

            // 无符号
            int byteSize = sizeof(byte);
            Console.WriteLine($"byte 所占的字节数：" + byteSize);
            int uintSize = sizeof(uint);
            Console.WriteLine($"uint 所占的字节数：" + uintSize);
            int ushortSize = sizeof(ushort);
            Console.WriteLine($"ushort 所占的字节数：" + ushortSize);
            int ulongSize = sizeof(ulong);
            Console.WriteLine($"ulong 所占的字节数：" + ulongSize);

            // 浮点数
            int floatSize = sizeof(float);
            Console.WriteLine($"float 所占的字节数：" + floatSize);
            int doubleSize = sizeof(double);
            Console.WriteLine($"double 所占的字节数：" + doubleSize);
            int decimalSize = sizeof(decimal);
            Console.WriteLine($"decimal 所占的字节数：" + decimalSize);

            // 特殊类型
            int boolSize = sizeof(bool);
            Console.WriteLine($"bool 所占的字节数：" + boolSize);
            int charSize = sizeof(char);
            Console.WriteLine($"char 所占的字节数：" + charSize);
            #endregion

            #region 变量的本质
            // 二进制存储，主要理解10进制和2进制之间的转换即可，8 4 2 1
            #endregion
        }
    }
}
