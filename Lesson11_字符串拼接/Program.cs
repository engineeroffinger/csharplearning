﻿using System;

namespace Lesson11_字符串拼接
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("字符串拼接");
            #region 字符串拼接方式1
            // 之前的算数运算符只是用数值类型变量进行数学运算
            // string不存在算数运算符不能计算，但可以通过+来进行拼接
            string str1 = "fab";
            str1 += "a" + 1 + true;
            Console.WriteLine(str1);
            str1 += 1 + 2 + 3 + 4;
            Console.WriteLine(str1); // 这里比较坑，因为右边都是同一类型，所以是先算数运算再拼接，直接拼接10
            str1 += 1 + 2 +" " + 3 + 4; // 这种情况也比较特别，只要出现字符串，那么后续的部分都会按照字符串的方式进行拼接
            Console.WriteLine(str1); // 3 34
            #endregion

            #region 字符串拼接方式2
            // 固定语法
            // string.Format("待拼接的内容",内容一,内容二,.....)
            // 拼接内容中的固定规则
            // 想要被拼接的内容用占位符替代 {数字} 数字：0~n 依次往后，占位符的意思，依次填充
            string str2 = string.Format("我是{0},我今年{1}，我想要{2}","cx",20,"好好学习");
            Console.WriteLine(str2);
            #endregion

            #region 字符串拼接方式3
            // 这种方式类似于Kotlin中的字符串模板，用于拼接变量
            int age = 10;
            string name = "cx";
            string str3 = $"我叫{name}，我今年{age}岁了";
            Console.WriteLine(str3);
            #endregion
        }
    }
}
