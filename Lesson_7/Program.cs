﻿using System;

namespace Lesson_7
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("类型转换");
            // 什么是类型转换
            // 类型转换就是不同变量类型之间的相互转换
            // 隐式转换的基本规则->不同类型之间自动转换
            // 大范围装小范围
            #region 相同大类型之间的转换
            // 有符号 long short int sbyte
            long l = 1;
            short s = 1;
            int i = 1;
            sbyte sb = 1;
            l = sb; // sbyte隐式转换成long类型
                    // sb = s;// 小的装大的报错
                    // 无符号同理

            // 浮点型 decimal double float
            decimal de = 1.1m;
            double d = 1.1d;
            float f = 1.2f;
            // decimal比较特殊，不能用于隐式转换，存不了double和float
            //de = d;
            //de = f;
            d = f; // float 可以隐式转换成double

            // 特殊类型之间不存在隐式转换
            #endregion

            #region 不同大类型之间的转换
            #region 无符号和有符号之间的转换
            // 无符号装有符号
            // 无符号 不能装负数
            byte b2 = 1;
            ushort us2 = 1;
            uint ui2 = 1;
            ulong ul2 = 1;
            // 有符号
            sbyte sb2 = 1;
            short s2 = 1;
            int i2 = 1;
            long l2 = 1;

            // 无符号装有符号
            // 有符号的变量不能隐式转换成无符号的
            //b2 = sb2;
            //us2 = s2;

            // 有符号装无符号
            // 基本原则还是大范围装小范围
            i2 = ui2; // 无符号的可能会超过有符号变量类型所能承载的范围
            i2 = b2; // 这种情况无论怎样，无符号的类型都不可能超过有符号变量的正数范围
            #endregion

            #region 浮点数和整数(有、无符号)之间
            // 浮点数装整数
            float f2 = 1.2f;
            double d2 = 1.2d;
            decimal de2 = 1.2m;

            // 浮点数是可以装载任何类型的整数
            f2 = l2;
            f2 = ul2;
            // decimal虽然不能隐式存储浮点型，但是可以装整型
            de2 = l2;
            de2 = ul2;
            // 整数装浮点数
            // 整数无法承载浮点数，也就是说浮点型无法隐式转换成整型
            i2 = f2;
            #endregion

            #region 特殊类型的转换
            // 特殊类型中只想说明char类型，char类型本质就是ASCII码，是整数
            char a = 'a';
            i2 = a;
            f2 = a;
            #endregion

            // 总结
            // 简单说就是高精度可以承载低精度
            // double->float->整数(有无符号)->char
            // decimal->整数(有无符号)->char
            #endregion
        }
    }
}
