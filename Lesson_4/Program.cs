﻿using System;

namespace Lesson_4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("变量命名遵循的规则");
            // 1.不能重名
            // 2.不能以数字开头
            // 3.不能使用程序关键字命名
            // 4.不能有特殊符号(下划线除外)
            int 2i = 1;
            int float = 2;
            string a~ = "fasdfa";
            // 不要用中文命名，英文最好见词思义
            string 我的名字 = "hhah";
            int length = 我的名字.Length;

            // 比较推荐的命名方式
            // 1.驼峰命名
            string myName = "cx";
            // 2.帕斯卡命名法-所有单词的首字母大写
            string MyName = "cx";

        }
    }
}
