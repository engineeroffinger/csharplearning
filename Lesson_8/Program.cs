﻿using System;

namespace Lesson_8
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("类型转换-显示转换");
            #region 括号强转
            // 作用 一般情况下，将高精度的类型强制转换为低精度
            // 语法：变量类型 变量名 = (变量类型)变量;
            // 注意：精度问题，范围问题

            // 相同大类的整型
            // 有符号整型
            sbyte sb = 1;
            short s = 1;
            int i = 1;
            long l = 1;

            //括号强转可能会出现范围问题，造成的异常，高精度或者高范围的向下强转很容易溢出
            s = (short)i;
            Console.WriteLine(s);
            // 无符号同理
            // 浮点数
            float f1 = 1.2f;
            double d1 = 1.2d;
            f1 = (float)d1;
            Console.WriteLine(f1); // 精度会丢失

            // 无符号和有符号
            // 相互间可以强转，但是注意范围，超出范围，得到的结果会很奇怪
            uint ui = 1;
            int i2 = 1;
            i2 = (int)ui;
            ui = (uint)i2;

            // 浮点和整数
            // 这种强转会丢失精度，并不会四舍五入
            i2 = (int)1.2f;
            Console.WriteLine(i2);
            // char类型和整数也支持强转
            int ascii = (int)'a';
            Console.WriteLine(ascii);
            char a = (char)ascii;
            Console.WriteLine(a);

            // bool和string不支持强转
            #endregion

            #region Parse方法强转
            // 作用：将字符串类型转换为对应的类型
            // 语法：变量类型.Parse("字符串");
            // 注意：字符串必须能够转换成对应类型，否则报错

            // 有符号
            int i4 = int.Parse("123");
            Console.WriteLine(i4);
            //int i5 = int.Parse("123.45"); //报错
            //short s3 = short.Parse("499999"); // 超出范围也会报错
            // 无符号，浮点型类似
            bool b5 = bool.Parse("true");
            Console.WriteLine(b5);
            #endregion

            #region Convert法
            // 作用：更准确地将各个类型之间进行相互转换
            // 语法：Convert.To目标类型(变量或者常量)
            // 注意：填写的变量或者常量必须正确，否则报错

            // 转字符串
            int a2 = Convert.ToInt32("12");
            Console.WriteLine(a2);
            // 精度更精确
            a2 = Convert.ToInt32(1.667f);
            Console.WriteLine(a2); // 会四舍五入
            // 特殊类型
            a2 = Convert.ToInt32(true);
            Console.WriteLine(a2);
            a2 = Convert.ToInt32(false);
            Console.WriteLine(a2);
            a2 = Convert.ToInt32('a');
            Console.WriteLine(a2);

            // 每一个类型都有一个对应的Convert方法
            sbyte sb3 = Convert.ToSByte("1");
            short s3 = Convert.ToInt16("1");
            long l3 = Convert.ToInt64("1");

            // 无符号
            byte b3 = Convert.ToByte("1");
            ushort us3 = Convert.ToUInt16("1");
            uint ui3 = Convert.ToUInt32("1");
            ulong ul3 = Convert.ToUInt64("1");

            float f3 = Convert.ToSingle("13.2"); // 单精度指的就是float
            double d3 = Convert.ToDouble("13.2");
            decimal de3 = Convert.ToDecimal("13.2");

            bool bo3 = Convert.ToBoolean("true");
            char ch3 = Convert.ToChar("a");
            string str3 = Convert.ToString(123);
            #endregion

            #region 其他类型转string
            // 作用：拼接打印
            // 语法：变量.ToString(); 
            string str4 = 1.ToString();
            string str5 = true.ToString();
            string str6 = 12.3f.ToString();
            // 当我们进行字符串拼接时，就自动会调用 ToString转成字符串
            Console.WriteLine("hello" + 123 + true);
            #endregion
        }
    }
}
