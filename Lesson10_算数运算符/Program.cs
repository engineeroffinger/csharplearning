﻿using System;

namespace Lesson10_算数运算符
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("算数运算符");
            #region 赋值符号
            // =
            // 关键知识点：先看右侧，再看左侧，把右侧的值赋值给左侧的变量

            string myName = "haha";
            #endregion

            #region 算数运算符
            // + - * / %
            int i = 1;
            i = i + 2;
            Console.WriteLine(i);

            // 连续运算 先算右侧结果，在赋值给左侧变量，用变量的值替换变量的位置即可
            i = 1 + 3 + 89 + i + i;
            Console.WriteLine(i);
            // 记住看见等号，先算右边就行
            int j = 1;
            j += 10 / 2 * 3; // 先算等号右边的内容，简化后变成 j += 15 -> j = j + 15
            Console.WriteLine(j);
            // 做一个比较经典的自增减运算题
            int a = 10, b = 20;
            int res = a++ + b++ + a++; // 10 + 20 + 11->41 第一个a++，用了之后，a就成了11
            Console.WriteLine(res);

            // 通过算术运算交换两个数的值
            int x = 10;
            int y = 9;
            x = x + y; // x = 10 + 9;
            y = x - y; // y = 10 + 9 - 9 = 10
            x = x - y; // x = 10 + 9 - 10 = 9
            Console.WriteLine($"交换后的数-x:{x}-y:{y}");
            #endregion
        }
    }
}
